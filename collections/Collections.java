package collections;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

public class Collections {
	public static void main(String[] args) {
		someCollections();
	}
	public  static void someCollections() {
		ArrayList<Integer> object1 = new ArrayList<Integer>();
		HashSet<String> object2 = new HashSet<String>();
		TreeSet<String> object3 = new TreeSet<String>();
		HashMap<Integer, String> object4 = new HashMap<Integer, String>();
		LinkedList<Integer> object5 = new LinkedList<Integer>();
//question 21. Below I'm adding to each collection.
		object1.add(0);
		object1.add(1);
		object1.add(2);
		
		object2.add("Hi");
		object2.add("I'm new here");
		object2.add("I Scream for Ice Cream");
		
		object3.add("Meteor Shower");
		object3.add("Has never been");
		object3.add("Meatier than this");
		
		object4.put(2343, "I'm hungry");
		object4.put(8383, "Bane");
		object4.put(111, "This might sting a bit");
		
		object5.add(123);
		object5.add(34343);
		object5.add(343);
	//question 23. Iterating each collection and printing their content
		System.out.println("ArrayList Example");
		for (int x = 0; x < object1.size(); x++) {
			System.out.println(object1.get(x));
		}
		System.out.println("HashSet Example");
		for (String s: object2){
			System.out.println(s);
		}
		System.out.println("Treeset example");
		for (String a: object3) {
			System.out.println(a);
		}
		System.out.println("HashMap example");
		for (int s:object4.keySet()) {
			System.out.println(s + ": " + object4.get(s));
		}
		System.out.println("LinkedList example");
		for(int z: object5) {
			System.out.println(z);
		}


	}

}