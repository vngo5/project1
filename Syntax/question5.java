package Syntax;

public class question5 {
	public static void main(String[] args) {
		calculate();
	}
	public static void calculate() {
		int number1 = 8;
		int number2 = 4;
		Math object = new Math(number1,number2);
		System.out.println(object.add(number1, number2));
		System.out.println(object.subtract(number1, number2));
		System.out.println(object.multiply(number1, number2));
		System.out.println(object.divide(number1, number2));
	}
}
