package question7;
//question 7
public class customer {
	public final static int MAX = 90;
	public static int counter = 1;
	public static int coupons = 0;
	private int num;
	private String name;
	
	public customer() {
		this.num = 0;
		this.name = "";
		counter++;
		coupons++;
	}
	public customer(int num, String name) {
		this();
		this.num = num;
		this.name = name;

	}
	public void setInfo(String n) {
		this.name = n;
	}
	public void setInfo(String n, int num) {
		this.name = n;
		this.num = num;
	}
	public String getInfo() {
		return name;
	}
	public int getNum() {
		return num;
	}
	public static int counting() {
		return counter;
	}
	public static int getCounter() {
		return counter;
	}
	public static int getCoupons() {
		return coupons;
	}
}
