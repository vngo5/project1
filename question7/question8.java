package question7;

import java.util.Scanner;
//question 8
public class question8 {
	public static void main(String[] args) {
		customer firstCustomer = new customer();
		//invoking overloaded constructor
		customer secondCustomer = new customer(25, "Bob");
		
		firstCustomer.setInfo("Johnson");
		secondCustomer.setInfo("Bob", 34);
		if (customer.getCounter() < customer.MAX){
			System.out.println("Current number is: " + customer.getCounter());
		}
		else {
			System.out.println("Max customers have been reached");
		}
		System.out.println("There have been a total of : " + customer.getCoupons() + " so far");
	}
}
