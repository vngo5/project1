package advanceCoding;

import java.util.Scanner;

//question 41
public class FizzBuzz {
	public static void main(String[] args) {
		int num =0;
		while (num < 50) {
			num++;
			if ((num % 3) == 0 && (num % 5) == 0) {
				System.out.println("FizzBuzz");
			}
			else if ((num % 5) == 0) {
				System.out.println("Buzz");
			}
			else if ((num % 3) == 0) {
				System.out.println("Fizz");
			}
		}
		
	}
}
