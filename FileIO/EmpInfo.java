package FileIO;

public class EmpInfo {
	private String EmployeeID;
	private String fName;
	private String lName;
	private String role;
	
	public EmpInfo() {
		this.EmployeeID = "";
		this.fName = "";
		this.lName = "";
		this.role = "";
	}
	public void setEmployeeID(String e) {
		this.EmployeeID = e;
	}
	public String getEmployeeID() {
		return EmployeeID;
	}
	public void setFName(String f) {
		this.fName = f;
	}
	public String getFName() {
		return fName;
	}
	public void setLName(String l) {
		this.lName = l;
	}
	public String getLName() {
		return lName;
	}
	public void setRole(String r) {
		this.role = r;
	}
	public String getRole() {
		return role;
	}
}
