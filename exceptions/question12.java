package exceptions;

public class question12 {
	public static void main(String[] args) {
		exceptionMethod();
		notHandled();
	}
	public static void exceptionMethod() throws customException {
		int x = -1;
		notHandled();
		question11 test = new question11(); 
		//question 12. 
		try {
			System.out.println("Starting the try block");
			test.setCofee(x);
			System.out.println("Ending try block");
			//question 15.
			System.exit(0);
		}
		catch(customException e) {
			System.out.println("Starting catch block");
			System.out.println("Error, the operation won't work");
			System.out.println("Ending catch block");
		}
		//question 14. Finally block executes
		finally {
			System.out.println("Have a nice and gracious day!");
		}
		
	}
//Question 13. Ducks exception
	public static void notHandled() throws customException {
		System.out.println("Exception has been ducked!");
	}
	
}
