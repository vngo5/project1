package easyCoding;

import java.util.InputMismatchException;
import java.util.Scanner;

public class KeyboardScanner {
	public static void main(String[] args) {
		inputMethod();
	}
	public static void inputMethod() {
		System.out.println("Enter in String value: ");
		Scanner scanner = new Scanner(System.in);
		String strValue= scanner.nextLine();
		int intValue = 0;
		double doubValue = 0.0;
			try {
				System.out.println("Enter in int: ");
				 intValue = scanner.nextInt();
			}
			catch (NumberFormatException e) {
				System.out.println("Error, enter valid value");
			}
			catch (InputMismatchException e) {
				System.out.println("Error, enter valid value");
			}
			try {
				System.out.println("Enter in double: ");
				doubValue = scanner.nextDouble();
			}
			catch (NumberFormatException e) {
				System.out.println("Error, enter valid value");
			}
			catch (InputMismatchException e) {
				System.out.println("Error, enter valid value");
			}
		System.out.println("String value: " + strValue);
		System.out.println("Int value: " + intValue);
		System.out.println("Double valeu: " + doubValue);
	}
}

