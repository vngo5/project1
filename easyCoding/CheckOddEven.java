package easyCoding;

import java.util.Scanner;

public class CheckOddEven {

	public static void main(String[] args) {
		evenOrOdd();
	}
//Question 33
	public static void evenOrOdd() {
		
		Scanner x = new Scanner(System.in);
		int Number = 0;
		boolean valid = false;
//Loops while valid is false;
		do {
			try {
				System.out.println("Enter in int: ");
				 Number = x.nextInt();
			}
			catch (NumberFormatException e) {
				System.out.println("Error, enter valid value");
			}
		}while (valid);
//Checks modulus of Number to determine if Number is even or odd
		if (Number%2 == 0) {
			System.out.println("Even number");
		}
		else {
			System.out.println("Odd Number");
		}
	}
}
