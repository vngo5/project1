package easyCoding;

import java.util.Scanner;

public class PrintNumberInWord {

	public static void main(String[] args) {
		printNumber();
		printNumberSwitch();
	}
//Question 34
	public static void printNumber() {
		
		Scanner x = new Scanner(System.in);
		int Number = 0;
		boolean valid = false;
//Loops while valid is false;
		
		do {
			try {
				System.out.println("Enter in int: ");
				 Number = x.nextInt();
			}
			catch (NumberFormatException e) {
				System.out.println("Error, enter valid value");
			}
//prints String based on number input.
			if (Number == 1) {
				System.out.println("ONE");
			}
			else if (Number == 2) {
				System.out.println("TWO");
			}else if (Number == 3) {
				System.out.println("THREE");
			}
			else if (Number == 4) {
				System.out.println("FOUR");
			}
			else if (Number == 5) {
				System.out.println("FIVE");
			}
			else if (Number == 6) {
				System.out.println("SIX");
			}
			else if (Number == 7) {
				System.out.println("SEVEN");
			}
			else if (Number == 8) {
				System.out.println("EIGHT");
			}else if (Number == 9) {
				System.out.println("NINE");
			}
			else {
				System.out.println("OTHER");
			}
		}while (valid);

	}
	public static void printNumberSwitch() {
		Scanner x = new Scanner(System.in);
		int Number2 = 0;
		boolean valid = false;
//Loops while valid is false;
		
		do {
			try {
				System.out.println("Enter in int: ");
				 Number2 = x.nextInt();
			}
			catch (NumberFormatException e) {
				System.out.println("Error, enter valid value");
			}
	//checks number input using switch case
			switch(Number2){
				case 1:
					System.out.println("ONE");
					break;
				case 2:
					System.out.println("TWO");
					break;
				case 3:
					System.out.println("THREE");
					break;
				case 4:
					System.out.println("FOUR");
					break;
				case 5:
					System.out.println("FIVE");
					break;
				case 6:
					System.out.println("SIX");
					break;
				case 7:
					System.out.println("SEVEN");
					break;
				case 8:
					System.out.println("EIGHT");
					break;
				case 9:
					System.out.println("NINE");
					break;
				default:
					System.out.println("OTHER");
					break;
			}
	}while(valid);
}
}
