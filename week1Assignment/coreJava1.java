package week1Assignment;


public class coreJava1 {
	public static void main(String[] arg) {
		calculate();
		greatestValue();
		printMethod();
	}
//Question 1
	public static void calculate() {
		int x = 8;
		int y = 4;
		System.out.println(x + y);
		System.out.println(x-y);
		System.out.println(x * y);
		System.out.println(x/y);
		
	}
//Question 2
	public static void greatestValue() {
		int num1 = 4;
		int num2 = 3;
		int num3 = 8;
		if(num1 > 4 && num1 > num3) {
			System.out.println(num1 + " is the greatest");
		}
		else if(num2 > num1 && num2 > num3) {
			System.out.println(num2 + " is the greatest");
		}
	}
//Question 3
	public static void printMethod() {
		boolean a = true;
		boolean b = false;
		System.out.println("The value of a is: " + !a);
		System.out.println("The value of a or b is: " + (a|b));
		System.out.println((!a &b) | (a &!b));
	}
	
}
