package multiThreading;

public class Multithreading extends Thread {

	private Thread process;
	private String thread;
	
	public Multithreading(String t) {
		this.thread = t;
	}


	public void run() {
		
//question 30 synchronized thread
		 String thread= Thread.currentThread().getName();
	      synchronized (thread) {
		try {
			
			System.out.println(thread + ": is running");
			for(int x = 0; x < 1000; x++) {
				Thread.sleep(50);
				System.out.println(thread + ": " + x);
			}
			System.out.println(thread + "Thread ending");
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	        }
	}
	public void start() {
		if(process== null) {
			process = new Thread(this,thread);
			process.start();
	
	}
	}
}