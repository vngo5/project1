package multiThreading;

public class ThreadTest {
	public static void main(String[] args) {
		testing();
	}
	public static void testing() {
		Multithreading test1 = new Multithreading("1st Thread");
		test1.start();
		Multithreading test2 = new Multithreading("2nd Thread");
		test2.start();
		Multithreading test3 = new Multithreading("3rd Thread");
		test3.start();
		
	}
}
/*
 * question 31. 
 * Sleep()is used to pause execution of a thread for a period of time	
 * Wait() causes thread to wait until the thread invokes notify method()
 * yield () will pause thread temporarily to allow other threads to run
 * suspend() suspends the thread and can only resume by invoking resume();
 * stop() will stop thread completely
 * resume() will continue suspended thread
 * interrupt() interrupts thread
 * notify() wakes up thread that was waiting
 * notifyAll() wakes up all threads
 */

