SELECT * FROM Employee;
SELECT * FROM Employee WHERE LASTNAME = 'King';
SELECT * FROM Employee WHERE FIRSTNAME = 'Andrew' AND REPORTSTO IS NULL;
SELECT * FROM Album ORDER BY title DESC;
SELECT FirstName FROM Customer ORDER BY city ASC;
INSERT INTO Genre (GenreId, Name) VALUES (123, 'John');
INSERT INTO Genre (GenreId, Name) VALUES (456, 'Ethan');
INSERT INTO Employee (EmployeeId,FirstName,LastName) VALUES (232, 'Johnny', 'Bravo');
INSERT INTO Employee (EmployeeId,FirstName,LastName) VALUES (233, 'Xander', 'Migo');
UPDATE Customer SET FirstName = 'Robert', LastName = 'Walter' WHERE FirstName = 'Aaron' ;
UPDATE Artist SET Name = 'CCR' WHERE Name = 'Creedence Clearwater Revival';
SELECT * FROM Invoice WHERE BillingAddress LIKE 'T%';
SELECT * FROM Invoice WHERE Total BETWEEN 15 AND 50;
SELECT * FROM Employee WHERE HireDate BETWEEN TO_DATE('2003-6-1', 'YYYY-MM-DD') AND TO_DATE('2004-3-1', 'YYYY-MM-DD');

SELECT Customer.FirstName, Invoice.InvoiceId
FROM Customer
INNER JOIN INVOICE
ON Customer.CustomerId=Invoice.CustomerId
ORDER BY Customer.FirstName;

SELECT Customer.FirstName, Customer.LastName, Invoice.InvoiceId, Invoice.total
FROM Customer
FULL OUTER JOIN Invoice
ON Customer.CustomerId=Invoice.CustomerId;

SELECT Album.Title, Artist.Name
FROM Album
RIGHT OUTER JOIN Artist
ON Album.ArtistId=Artist.ArtistId;

SELECT * 
FROM Album
CROSS JOIN Artist ORDER BY Name ASC;



        