package advanceConcepts;

import java.util.StringTokenizer;
//question 17
public class tokenizer {
	public static void main(String[] args) {
		String str = "pickles:ketchup:mustard:onion";
		StringTokenizer token = new StringTokenizer(str, ":");
		while(token.hasMoreTokens()){
			System.out.println(token.nextToken());
		}
	}
}
