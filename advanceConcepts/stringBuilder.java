package advanceConcepts;

public class stringBuilder {
	public static void main(String[] args) {
		StringBuilder strObject = new StringBuilder("This is my String!");
		appendingString(strObject);
		deleteMethod(strObject);
		setCharAt(strObject);
	}
//question 16
	public static void appendingString(StringBuilder strObject) {
		
		System.out.println(strObject.append("Here is more String!"));
	}
	public static void deleteMethod(StringBuilder strObject) {
		System.out.println(strObject.deleteCharAt(10));
	}
	public static void setCharAt(StringBuilder strObject) {
		System.out.println(strObject.insert(10, " "));
	}
}
