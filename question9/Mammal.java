package question9;

public abstract class Mammal implements Animal {
	private String eat;
	private static String walk;
	
	public Mammal() {
		this.eat = "";
		walk = "";
	}
	public Mammal(String e) {
		this();
		this.eat = e;
	}
	public void eat() {
		System.out.println("Animals eat either meat, plants, or sometimes both");
	}
	public String getEat() {
		return eat;
	}
	public static String getWalk() {
		walk = "The various mammalian creatures have a unique way of walking around to places";
		return walk;
	}
}
