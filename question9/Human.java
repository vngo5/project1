package question9;

public class Human extends Mammal{
	private String move;
	private static String walk;
	
	public Human() {
		super();
		walk = "";
		move = "";
	}
	public static String walk() {
		walk = "Humans typically walk on two legs";
		return walk;
	}
	@Override
	public void move() {
		System.out.println("but they use different forms of transportations including car, boat, planes,"
				+ "and vice versa");
	}
	@Override
	public void eat() {
		System.out.println("Humans eat a lot of different things!");
		
	}
}
