CREATE TABLE workers (
  empID int NOT NULL,
  empName varchar2(50),
  empSalary Number(5,2)NOT NULL,
  CONSTRAINT pk_emID PRIMARY KEY(empID)
);
CREATE TABLE cashierPerson(
    empID int,
    salary Number(5,2) NOT NULL,
    CONSTRAINT fk_emplID FOREIGN KEY(empID)REFERENCES workers(empID)
);
Create Table serveringPerson (
    empID int,
    empSalary Number (5,2) NOT NULL,
    orderID int,
    tip Number(5,2),
    CONSTRAINT fk_emdID FOREIGN KEY (empID)REFERENCES workers(empID),
    CONSTRAINT fk_oaID FOREIGN KEY (orderID)REFERENCES customerOrder(orderID)
    );
Create Table cooker (
   empID int, 
   empSalary Number(5,2),
   orderID int,
   CONSTRAINT fk_emID FOREIGN KEY (empID)REFERENCES workers(empID),
   CONSTRAINT fk_oatID FOREIGN KEY (orderID)REFERENCES customerOrder(orderID)
);
CREATE TABLE customerOrder (
    orderID int NOT NULL,
    orderName varchar2(50),
    ordercost Number(5,2),
    customerName varchar2(50),
    CONSTRAINT pk_ooiID PRIMARY KEY(orderID)
);
Create Table table1 (
    tableID int NOT NULL,
    tableCapacity int NOT NULL,
    orderID int,
    CONSTRAINT pk_tID PRIMARY KEY(tableID),
    CONSTRAINT fk_oID FOREIGN KEY(orderID) REFERENCES customerOrder(orderID)
);

Create Table deliveryDriver (
    empID int,
    empSalary Number (5,2),
    tip Number (7,2),
    orderID int NOT NULL,
    CONSTRAINT fk_emPizID FOREIGN KEY (empID)REFERENCES workers(empID),
    CONSTRAINT fk_oaodID FOREIGN KEY (orderID)REFERENCES customerOrder(orderID)
);
CREATE TABLE janitor (
  empID int,
  empSalary Number(4,2),
  CONSTRAINT fk_femID FOREIGN KEY (empID)REFERENCES workers(empID)
);
CREATE TABLE ITSpecialist (
  empID int, 
  empSalary Number(5,2),
   CONSTRAINT fk_aemID FOREIGN KEY (empID)REFERENCES workers(empID)
);
CREATE TABLE Reservee (
  empID int,
  empSalary Number(5,2),
  CONSTRAINT fk_semID FOREIGN KEY (empID)REFERENCES workers(empID)
);

SELECT * FROM customerOrder;
ALTER TABLE customerOrder
ADD customerName varchar2(50);
ALTER table cooker add primary key (empID,orderID);
ALTER TABLE cooker
ADD CONSTRAINT pk_PersonID PRIMARY KEY (empSalary);
INSERT INTO workers(empID,empName,empSalary) Values(2,'Mith',20.00);

GRANT ALL ON workers TO public;
GRANT ALL ON cooker TO public;



SET DEFINE OFF;
CREATE OR REPLACE TRIGGER newSalary
  AFTER
    INSERT OR
    UPDATE OF empSalary 
  ON workers
  Declare 
  startingSalary number;
BEGIN

  CASE
    WHEN INSERTING THEN
      DBMS_OUTPUT.PUT_LINE('worker hasstarted working today ');
    WHEN UPDATING('empSalary') THEN
      DBMS_OUTPUT.PUT_LINE('worker has a new salary after working hard ');
  END CASE;
END;

UPDATE workers
SET empSalary=40
WHERE empID=1;
INSERT INTO workers Values(3,'Joe',70.00);
UPDATE workers SET empSalary = 90 WHERE empID = 3;
UPDATE workers SET empSalary = 80 WHERE empID = 3;
INSERT INTO workers Values(4,'Joey',80.00);
SELECT * FROM workers;

ALTER TRIGGER newSalary ENABLE;
INSERT INTO workers VALUES(5, 'Hobo', 80.00);
INSERT INTO workers VALUES (null, 'Diego', 30.00);
INSERT INTO  cooker(empID, empSalary, orderID) VALUES(1,20.00,null);
UPDATE workers SET empSalary = 95 WHERE empID = 3;