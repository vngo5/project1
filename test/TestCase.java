package test;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCase {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("@BeforeClass: Triggered");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("@AfterClass: Triggered");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("@BeforeClass: Triggered");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("@AfterClass: Triggered");
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}
	@Test
	public void test1() {
		System.out.println("Test1(): Triggered");
		fail("Not yet implemented");
	}
	@Test
	public void test2() {
		System.out.println("Test2(): Triggered");
		fail("Not yet implemented");
	}
	@Test
	public void test3() {
		System.out.println("Test3(): Triggered");
		fail("Not yet implemented");
	}

}
